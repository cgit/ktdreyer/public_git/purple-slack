%global plugin_name slack

%global commit0 c10cc72e441b3edbeee5b00049a4a1c977d746e6
%global shortcommit0 %(c=%{commit0}; echo ${c:0:7})
%global date 20190613

Name: purple-%{plugin_name}
Version: 0
Release: 1.%{date}git%{shortcommit0}%{?dist}
Summary: Slack plugin for Pidgin/Adium/libpurple

License: GPLv2
URL: https://github.com/dylex/slack-libpurple
Source0: https://github.com/dylex/slack-libpurple/archive/%{commit0}.tar.gz#/slack-libpurple-%{shortcommit0}.tar.gz

BuildRequires: pkgconfig(glib-2.0)
BuildRequires: pkgconfig(purple)
BuildRequires: pkgconfig(json-glib-1.0)
BuildRequires: pkgconfig(zlib)
BuildRequires: gcc

%package -n pidgin-%{plugin_name}
Summary: Adds pixmaps, icons and smileys for Steam protocol
BuildArch: noarch
Requires: %{name} = %{?epoch:%{epoch}:}%{version}-%{release}
Requires: pidgin

%description
Adds support for Slack to Pidgin, Adium, Finch and other libpurple
based messengers.

%description -n pidgin-%{plugin_name}
Adds pixmaps, icons and smileys for Slack protocol.

%prep
%setup -qn slack-libpurple-%{commit0}

%build
export CFLAGS="%{optflags}"
export LDFLAGS="%{__global_ldflags} -lz"
%make_build

%install
%make_install

%files
%{_libdir}/purple-2/libslack.so
%doc README.md
%license COPYING

%files -n pidgin-%{plugin_name}
%{_datadir}/pixmaps/pidgin/protocols/*/slack.png

%changelog
* Thu Jun 13 2019 Ken Dreyer <ktdreyer@ktdreyer.com> - 0-1.20190613gitc10cc72
- update to latest git snapshot

* Tue Jan 15 2019 Ken Dreyer <ktdreyer@ktdreyer.com> - 0-1.20190116git7e5c16f
- update to latest git snapshot

* Mon Nov 05 2018 Ken Dreyer <ktdreyer@ktdreyer.com> - 0-1.20181105git39c7b5b
- update to latest git snapshot

* Tue Jun 26 2018 Ken Dreyer <ktdreyer@ktdreyer.com> - 0-1.20180625gitc4a7207
- Initial package
